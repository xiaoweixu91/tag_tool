# -*- coding: utf-8 -*-

from flask import render_template, flash, redirect, jsonify, session, request, abort
from app import app, mysql, es, login_manager
from .forms import LoginForm, SearchForm, Tag
import time, flask_login, requests
from models import User
from elasticsearch.exceptions import ElasticsearchException
import urllib3, os
from werkzeug import secure_filename
from logging import Formatter, FileHandler
import tinys3,json

urllib3.disable_warnings()

recipes_index = 'sweden_recipes'
def keywordSearch(data):
    if data:

        match_q = {"multi_match": {"query": data,
                                   "fields": ["name^10", "name.grams^2", "special_diet^5", "short_description",
                                              "extra_info", "cuisine", "course"], "operator": "and",
                                   "analyzer": "standard", "type": "cross_fields"}}
    else:
        match_q = {'match_all': {}}

    page = 1
    per_page = 20

    query = {
        "query": {
            "function_score": {
                'query': match_q,
                "functions": [
                    {
                        "random_score": {
                            "seed": int(time.time())
                            # "seed": 1
                        }
                    }
                ]
            },

        },
        "from": (page - 1) * per_page,
        "size": per_page
    }
    try:
        recipes = es.search(index='sweden_recipes', doc_type="recipe", body=query)

    except ElasticsearchException:
        return jsonify({'status': 'SERVER_ERROR', 'message': 'Error On Server.'})
    # search_form.search.data=''
    return recipes['hits']['hits']


def tagSearch(data):
    if data:

        match_q = {'match': {"categories": data}}
    else:
        match_q = {'match_all': {}}

    page = 1
    per_page = 20

    query = {
        "query": {
            "function_score": {
                'query': match_q,
                "functions": [
                    {
                        "random_score": {
                            "seed": int(time.time())
                            # "seed": 1
                        }
                    }
                ]
            },

        },
        "from": (page - 1) * per_page,
        "size": per_page
    }
    try:
        recipes = es.search(index='sweden_recipes', doc_type="recipe", body=query)

    except ElasticsearchException:
        return jsonify({'status': 'SERVER_ERROR', 'message': 'Error On Server.'})

    return recipes['hits']['hits']


##############################################/LOGIN######################################################


@app.route('/', methods=['GET', 'POST'])
@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        if form.id.data == 'Admin':
            user = User(form.id.data)
            flask_login.login_user(user)
            flash('Login requested for ID="%s", remember_me=%s' %
                  (form.id.data, str(form.remember_me.data)))
            return redirect('/index')

        else:
            flash('Your Id is not correct.Please try Again!')

    return render_template('LoginForm.html',
                           form=form)


@app.route('/logout')
def logout():
    flask_login.logout_user()
    return redirect('/')


@app.route('/index', methods=['GET', 'POST'])
@flask_login.login_required
def index():
    search_form = SearchForm()

    if search_form.validate_on_submit():
        # print search_form.keyword.data
        data = search_form.search.data

        if search_form.keyword.data:
            results = keywordSearch(data)

        elif search_form.tag.data:
            results = tagSearch(data)

        return render_template('index.html', form=search_form, recipes=results)

    match_q = {'match_all': {}}
    page = 1
    per_page = 50

    query = {
        "query": {
            "function_score": {
                'query': match_q,
                "functions": [
                    {
                        "random_score": {
                            "seed": int(time.time())
                            # "seed": 1
                        }
                    }
                ]
            },

        },
        "from": (page - 1) * per_page,
        "size": per_page
    }
    try:
        recipes = es.search(index='sweden_recipes', doc_type="recipe", body=query)

    except ElasticsearchException:
        return jsonify({'status': 'SERVER_ERROR', 'message': 'Error On Server.'})

    return render_template('index.html', form=search_form, recipes=recipes['hits']['hits'])


@app.route('/recipe/<uniqid>', methods=['GET', 'POST'])
@flask_login.login_required
def recipe(uniqid):
    query = {
        "_source": {
            "include": ["course", "credit_text", "short_description", "images", "ingredients", "name", "prep_time",
                        "servings",
                        "source_url", "special_diet", "uniqid", "num_ingredients", "categories", "steps", "token"]
        },
        "query": {
            "match": {"uniqid": uniqid}
        }
    }

    try:
        result = es.search(index=recipes_index, doc_type="recipe", body=query)
    except ElasticsearchException:
        return jsonify({"status": "SERVER_ERROR", "message": "Error On Server."})

    items = result["hits"]["hits"]
    # print items
    item = result["hits"]["hits"][0]
    item["favorite"] = False
    item["_source"]["index"] = item["_index"]
    item["_source"]["id"] = item["_id"]
    # print item
    # tags = item['_source']['categories']

    tag = Tag()

    return render_template('recipe_detail.html', recipe=item, tag=tag)


@app.route('/foo/<uniqid>', methods=['GET', 'POST'])
@flask_login.login_required
def foo(uniqid):
    data = request.form.to_dict()
    id = data['id']
    tags = data['tags']
    credit = data['credit']
    tags = re.split(',', tags)
    print '#' * 50
    print tags, id
    print '#' * 50
    if credit.lower().__contains__('koket'):
        es.update(index='sweden_recipes_koket', doc_type='recipe', id=id, body={"doc": {"categories": tags}})
    elif credit.lower().__contains__('mittkok'):
        es.update(index='sweden_recipes_mittkok', doc_type='recipe', id=id, body={"doc": {"categories": tags}})
    elif credit.lower().__contains__('tasteline'):
        es.update(index='sweden_recipes_tasteline', doc_type='recipe', id=id, body={"doc": {"categories": tags}})
    else:
        es.update(index='sweden_recipes_upload_recipe', doc_type='recipe', id=id, body={"doc": {"categories": tags}})

    return 'success'



@app.route('/tag', methods=['POST'])
@flask_login.login_required
def tag():
    data = request.get_data()
    results = tagSearch(data)
    search_form = SearchForm()
    return render_template('index.html', form=search_form, recipes=results)


@app.route('/create_category', methods=['POST', 'GET'])
@flask_login.login_required
def create_category():
    # category_form = CategoryForm()


    return render_template('create_category.html')


@app.route('/add_tag_in_category', methods=['GET', 'POST'])
@flask_login.login_required
def create_tag():
    return render_template('create_tag.html')

@app.route('/change_order',methods=['GET','POST'])
@flask_login.login_required
def change_order():
    r= requests.get(app.config['ROOT_URL']+"order_category")
    data = json.loads(r.content)
    return render_template('change_order.html',categories=data,image_url=app.config['IMAGE_URL'])

@app.route('/edit_category_name')
@flask_login.login_required
def edit_category_name():
    return render_template('edit_category_name.html')


