from flask_wtf import FlaskForm
from wtforms import StringField,BooleanField,SubmitField
from wtforms.validators import DataRequired

class LoginForm(FlaskForm):
    id = StringField('id',validators=[DataRequired()])
    remember_me = BooleanField('remember_me',default=False)

class SearchForm(FlaskForm):
    search = StringField('search', validators=[DataRequired()])
    keyword = SubmitField(label='Keyword Search')
    tag = SubmitField(label='Tag Search')

class Tag(FlaskForm):
    tag = StringField('tag',validators=[DataRequired])

# class CategoryForm(FlaskForm):
#     usa_name = StringField('usa_name',validators=[DataRequired])
#     sv_name = StringField('sv_name',validators=[DataRequired])
#     image_name = StringField('image_name',validators=[DataRequired])
