from flask import Flask
from flask_mysqldb import MySQL
from elasticsearch import Elasticsearch
import flask_login
app = Flask(__name__)
app.config.from_object('config')
mysql = MySQL(app)
# mysql.init_app(app)
es = Elasticsearch(app.config['ES_DB'],verify_certs=False)
login_manager = flask_login.LoginManager()
login_manager.init_app(app)

from app import views
