# from app import mysql
import flask_login
from app import login_manager

users = ['Admin']

class User(flask_login.UserMixin):
    def __init__(self,id):
        self.id = id

@login_manager.user_loader
def user_loader(id):
    if id not in users:
        return

    user = User(id)
    # user.id = id
    return user


@login_manager.request_loader
def request_loader(request):
    id = request.form.get('id')
    if id not in users:
        return

    user = User(id)
    # user.id = id

    user.is_authenticated = request.form['id']